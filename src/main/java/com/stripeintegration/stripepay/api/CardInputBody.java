package com.stripeintegration.stripepay.api;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Card model request
 */
@ApiModel(description = "Card model request")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class CardInputBody {
	@JsonProperty("email")
	private String email = null;

	@JsonProperty("stripeToken")
	private StripeTokenResponse stripeToken = null;

	public CardInputBody email(String email) {
		this.email = email;
		return this;
	}

	/**
	 * Get email
	 * 
	 * @return email
	 **/
	@ApiModelProperty(value = "")

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CardInputBody stripeToken(StripeTokenResponse stripeToken) {
		this.stripeToken = stripeToken;
		return this;
	}

	/**
	 * Get stripeToken
	 * 
	 * @return stripeToken
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public StripeTokenResponse getStripeToken() {
		return stripeToken;
	}

	public void setStripeToken(StripeTokenResponse stripeToken) {
		this.stripeToken = stripeToken;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CardInputBody card = (CardInputBody) o;
		return Objects.equals(this.email, card.email) && Objects.equals(this.stripeToken, card.stripeToken);
	}

	@Override
	public int hashCode() {
		return Objects.hash(email, stripeToken);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Card {\n");

		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    stripeToken: ").append(toIndentedString(stripeToken)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

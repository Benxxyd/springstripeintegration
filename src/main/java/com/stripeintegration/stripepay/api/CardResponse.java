package com.stripeintegration.stripepay.api;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Handling payment responses
 */
@ApiModel(description = "Handling payment responses")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class CardResponse {
	@JsonProperty("status")
	private String status = null;

	@JsonProperty("message")
	private String message = null;

	public CardResponse status(String status) {
		this.status = status;
		return this;
	}

	/**
	 * Card status description
	 * 
	 * @return status
	 **/
	@ApiModelProperty(example = "Card handling can not be executed", value = "Card status description")

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CardResponse message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Card status message
	 * 
	 * @return message
	 **/
	@ApiModelProperty(example = "Success", value = "Card status message")

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CardResponse cardResponse = (CardResponse) o;
		return Objects.equals(this.status, cardResponse.status) && Objects.equals(this.message, cardResponse.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(status, message);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CardResponse {\n");

		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

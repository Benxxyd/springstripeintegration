package com.stripeintegration.stripepay.api;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Set default card model
 */
@ApiModel(description = "Set default card model")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class DefaultCard {
	@JsonProperty("email")
	private String email = null;

	@JsonProperty("stripeCardId")
	private String stripeCardId = null;

	public DefaultCard email(String email) {
		this.email = email;
		return this;
	}

	/**
	 * Get email
	 * 
	 * @return email
	 **/
	@ApiModelProperty(value = "")

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DefaultCard stripeCardId(String stripeCardId) {
		this.stripeCardId = stripeCardId;
		return this;
	}

	/**
	 * Get stripeCardId
	 * 
	 * @return stripeCardId
	 **/
	@ApiModelProperty(example = "car_Pkfagaagagg284fa2g15a", value = "")

	public String getStripeCardId() {
		return stripeCardId;
	}

	public void setStripeCardId(String stripeCardId) {
		this.stripeCardId = stripeCardId;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DefaultCard defaultCard = (DefaultCard) o;
		return Objects.equals(this.email, defaultCard.email)
				&& Objects.equals(this.stripeCardId, defaultCard.stripeCardId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(email, stripeCardId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class DefaultCard {\n");

		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    stripeCardId: ").append(toIndentedString(stripeCardId)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

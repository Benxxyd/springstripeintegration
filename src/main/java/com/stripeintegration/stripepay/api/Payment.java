package com.stripeintegration.stripepay.api;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Required body input for charge
 */
@ApiModel(description = "Required body input for charge")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class Payment {
	@JsonProperty("email")
	private String email = null;

	@JsonProperty("price")
	private Double price = null;

	@JsonProperty("stripeToken")
	private StripeTokenResponse stripeToken = null;

	public Payment email(String email) {
		this.email = email;
		return this;
	}

	/**
	 * Email of the buyer
	 * 
	 * @return email
	 **/
	@ApiModelProperty(example = "example@hotmail.com", value = "Email of the buyer")

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Payment price(Double price) {
		this.price = price;
		return this;
	}

	/**
	 * Amount which will be charged
	 * 
	 * @return price
	 **/
	@ApiModelProperty(example = "2.15", value = "Amount which will be charged")

	@Valid
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Payment stripeToken(StripeTokenResponse stripeToken) {
		this.stripeToken = stripeToken;
		return this;
	}

	/**
	 * Get stripeToken
	 * 
	 * @return stripeToken
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public StripeTokenResponse getStripeToken() {
		return stripeToken;
	}

	public void setStripeToken(StripeTokenResponse stripeToken) {
		this.stripeToken = stripeToken;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Payment payment = (Payment) o;
		return Objects.equals(this.email, payment.email) && Objects.equals(this.price, payment.price)
				&& Objects.equals(this.stripeToken, payment.stripeToken);
	}

	@Override
	public int hashCode() {
		return Objects.hash(email, price, stripeToken);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Payment {\n");

		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    price: ").append(toIndentedString(price)).append("\n");
		sb.append("    stripeToken: ").append(toIndentedString(stripeToken)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

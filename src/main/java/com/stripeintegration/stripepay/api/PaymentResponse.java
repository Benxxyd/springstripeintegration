package com.stripeintegration.stripepay.api;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Handling payment responses
 */
@ApiModel(description = "Handling payment responses")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class PaymentResponse {
	@JsonProperty("status")
	private String status = null;

	@JsonProperty("message")
	private String message = null;

	public PaymentResponse status(String status) {
		this.status = status;
		return this;
	}

	/**
	 * Payment status description
	 * 
	 * @return status
	 **/
	@ApiModelProperty(example = "Status code 200", value = "Payment status description")

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PaymentResponse message(String message) {
		this.message = message;
		return this;
	}

	/**
	 * Payment status message
	 * 
	 * @return message
	 **/
	@ApiModelProperty(example = "Success", value = "Payment status message")

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentResponse paymentResponse = (PaymentResponse) o;
		return Objects.equals(this.status, paymentResponse.status)
				&& Objects.equals(this.message, paymentResponse.message);
	}

	@Override
	public int hashCode() {
		return Objects.hash(status, message);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentResponse {\n");

		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

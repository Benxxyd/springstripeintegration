package com.stripeintegration.stripepay.api;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Stripe card object
 */
@ApiModel(description = "Stripe card object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class StripeCardResponse {
	@JsonProperty("id")
	private String id = null;

	@JsonProperty("object")
	private String object = null;

	@JsonProperty("address_city")
	private Object addressCity = null;

	@JsonProperty("address_country")
	private Object addressCountry = null;

	@JsonProperty("address_line1")
	private Object addressLine1 = null;

	@JsonProperty("address_line1_check")
	private Object addressLine1Check = null;

	@JsonProperty("address_line2")
	private Object addressLine2 = null;

	@JsonProperty("address_state")
	private Object addressState = null;

	@JsonProperty("address_zip")
	private Object addressZip = null;

	@JsonProperty("address_zip_check")
	private Object addressZipCheck = null;

	@JsonProperty("brand")
	private String brand = null;

	@JsonProperty("country")
	private String country = null;

	@JsonProperty("cvc_check")
	private Object cvcCheck = null;

	@JsonProperty("dynamic_last4")
	private Object dynamicLast4 = null;

	@JsonProperty("exp_month")
	private BigDecimal expMonth = null;

	@JsonProperty("exp_year")
	private BigDecimal expYear = null;

	@JsonProperty("fingerprint")
	private String fingerprint = null;

	@JsonProperty("funding")
	private String funding = null;

	@JsonProperty("last4")
	private String last4 = null;

	@JsonProperty("metadata")
	private Object metadata = null;

	@JsonProperty("name")
	private Object name = null;

	@JsonProperty("tokenization_method")
	private Object tokenizationMethod = null;

	public StripeCardResponse id(String id) {
		this.id = id;
		return this;
	}

	/**
	 * Stripe card token ID
	 * 
	 * @return id
	 **/
	@ApiModelProperty(example = "card_1FijQuLLyi74k9eCSYt7ohe2", value = "Stripe card token ID")

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StripeCardResponse object(String object) {
		this.object = object;
		return this;
	}

	/**
	 * Get object
	 * 
	 * @return object
	 **/
	@ApiModelProperty(value = "")

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public StripeCardResponse addressCity(Object addressCity) {
		this.addressCity = addressCity;
		return this;
	}

	/**
	 * Get addressCity
	 * 
	 * @return addressCity
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressCity() {
		return addressCity;
	}

	public void setAddressCity(Object addressCity) {
		this.addressCity = addressCity;
	}

	public StripeCardResponse addressCountry(Object addressCountry) {
		this.addressCountry = addressCountry;
		return this;
	}

	/**
	 * Get addressCountry
	 * 
	 * @return addressCountry
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressCountry() {
		return addressCountry;
	}

	public void setAddressCountry(Object addressCountry) {
		this.addressCountry = addressCountry;
	}

	public StripeCardResponse addressLine1(Object addressLine1) {
		this.addressLine1 = addressLine1;
		return this;
	}

	/**
	 * Get addressLine1
	 * 
	 * @return addressLine1
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(Object addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public StripeCardResponse addressLine1Check(Object addressLine1Check) {
		this.addressLine1Check = addressLine1Check;
		return this;
	}

	/**
	 * Get addressLine1Check
	 * 
	 * @return addressLine1Check
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressLine1Check() {
		return addressLine1Check;
	}

	public void setAddressLine1Check(Object addressLine1Check) {
		this.addressLine1Check = addressLine1Check;
	}

	public StripeCardResponse addressLine2(Object addressLine2) {
		this.addressLine2 = addressLine2;
		return this;
	}

	/**
	 * Get addressLine2
	 * 
	 * @return addressLine2
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(Object addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public StripeCardResponse addressState(Object addressState) {
		this.addressState = addressState;
		return this;
	}

	/**
	 * Get addressState
	 * 
	 * @return addressState
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressState() {
		return addressState;
	}

	public void setAddressState(Object addressState) {
		this.addressState = addressState;
	}

	public StripeCardResponse addressZip(Object addressZip) {
		this.addressZip = addressZip;
		return this;
	}

	/**
	 * Get addressZip
	 * 
	 * @return addressZip
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressZip() {
		return addressZip;
	}

	public void setAddressZip(Object addressZip) {
		this.addressZip = addressZip;
	}

	public StripeCardResponse addressZipCheck(Object addressZipCheck) {
		this.addressZipCheck = addressZipCheck;
		return this;
	}

	/**
	 * Get addressZipCheck
	 * 
	 * @return addressZipCheck
	 **/
	@ApiModelProperty(value = "")

	public Object getAddressZipCheck() {
		return addressZipCheck;
	}

	public void setAddressZipCheck(Object addressZipCheck) {
		this.addressZipCheck = addressZipCheck;
	}

	public StripeCardResponse brand(String brand) {
		this.brand = brand;
		return this;
	}

	/**
	 * Type of the card
	 * 
	 * @return brand
	 **/
	@ApiModelProperty(example = "Visa", value = "Type of the card")

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public StripeCardResponse country(String country) {
		this.country = country;
		return this;
	}

	/**
	 * Country
	 * 
	 * @return country
	 **/
	@ApiModelProperty(example = "US", value = "Country")

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public StripeCardResponse cvcCheck(Object cvcCheck) {
		this.cvcCheck = cvcCheck;
		return this;
	}

	/**
	 * Get cvcCheck
	 * 
	 * @return cvcCheck
	 **/
	@ApiModelProperty(value = "")

	public Object getCvcCheck() {
		return cvcCheck;
	}

	public void setCvcCheck(Object cvcCheck) {
		this.cvcCheck = cvcCheck;
	}

	public StripeCardResponse dynamicLast4(Object dynamicLast4) {
		this.dynamicLast4 = dynamicLast4;
		return this;
	}

	/**
	 * Get dynamicLast4
	 * 
	 * @return dynamicLast4
	 **/
	@ApiModelProperty(value = "")

	public Object getDynamicLast4() {
		return dynamicLast4;
	}

	public void setDynamicLast4(Object dynamicLast4) {
		this.dynamicLast4 = dynamicLast4;
	}

	public StripeCardResponse expMonth(BigDecimal expMonth) {
		this.expMonth = expMonth;
		return this;
	}

	/**
	 * Expiration month of the card
	 * 
	 * @return expMonth
	 **/
	@ApiModelProperty(example = "4", value = "Expiration month of the card")

	@Valid
	public BigDecimal getExpMonth() {
		return expMonth;
	}

	public void setExpMonth(BigDecimal expMonth) {
		this.expMonth = expMonth;
	}

	public StripeCardResponse expYear(BigDecimal expYear) {
		this.expYear = expYear;
		return this;
	}

	/**
	 * Expiration year of the card
	 * 
	 * @return expYear
	 **/
	@ApiModelProperty(example = "2020", value = "Expiration year of the card")

	@Valid
	public BigDecimal getExpYear() {
		return expYear;
	}

	public void setExpYear(BigDecimal expYear) {
		this.expYear = expYear;
	}

	public StripeCardResponse fingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
		return this;
	}

	/**
	 * Get fingerprint
	 * 
	 * @return fingerprint
	 **/
	@ApiModelProperty(value = "")

	public String getFingerprint() {
		return fingerprint;
	}

	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}

	public StripeCardResponse funding(String funding) {
		this.funding = funding;
		return this;
	}

	/**
	 * Get funding
	 * 
	 * @return funding
	 **/
	@ApiModelProperty(value = "")

	public String getFunding() {
		return funding;
	}

	public void setFunding(String funding) {
		this.funding = funding;
	}

	public StripeCardResponse last4(String last4) {
		this.last4 = last4;
		return this;
	}

	/**
	 * Last 4 digits of the card
	 * 
	 * @return last4
	 **/
	@ApiModelProperty(example = "4242", value = "Last 4 digits of the card")

	public String getLast4() {
		return last4;
	}

	public void setLast4(String last4) {
		this.last4 = last4;
	}

	public StripeCardResponse metadata(Object metadata) {
		this.metadata = metadata;
		return this;
	}

	/**
	 * Get metadata
	 * 
	 * @return metadata
	 **/
	@ApiModelProperty(value = "")

	public Object getMetadata() {
		return metadata;
	}

	public void setMetadata(Object metadata) {
		this.metadata = metadata;
	}

	public StripeCardResponse name(Object name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "")

	public Object getName() {
		return name;
	}

	public void setName(Object name) {
		this.name = name;
	}

	public StripeCardResponse tokenizationMethod(Object tokenizationMethod) {
		this.tokenizationMethod = tokenizationMethod;
		return this;
	}

	/**
	 * Get tokenizationMethod
	 * 
	 * @return tokenizationMethod
	 **/
	@ApiModelProperty(value = "")

	public Object getTokenizationMethod() {
		return tokenizationMethod;
	}

	public void setTokenizationMethod(Object tokenizationMethod) {
		this.tokenizationMethod = tokenizationMethod;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		StripeCardResponse stripeCardResponse = (StripeCardResponse) o;
		return Objects.equals(this.id, stripeCardResponse.id) && Objects.equals(this.object, stripeCardResponse.object)
				&& Objects.equals(this.addressCity, stripeCardResponse.addressCity)
				&& Objects.equals(this.addressCountry, stripeCardResponse.addressCountry)
				&& Objects.equals(this.addressLine1, stripeCardResponse.addressLine1)
				&& Objects.equals(this.addressLine1Check, stripeCardResponse.addressLine1Check)
				&& Objects.equals(this.addressLine2, stripeCardResponse.addressLine2)
				&& Objects.equals(this.addressState, stripeCardResponse.addressState)
				&& Objects.equals(this.addressZip, stripeCardResponse.addressZip)
				&& Objects.equals(this.addressZipCheck, stripeCardResponse.addressZipCheck)
				&& Objects.equals(this.brand, stripeCardResponse.brand)
				&& Objects.equals(this.country, stripeCardResponse.country)
				&& Objects.equals(this.cvcCheck, stripeCardResponse.cvcCheck)
				&& Objects.equals(this.dynamicLast4, stripeCardResponse.dynamicLast4)
				&& Objects.equals(this.expMonth, stripeCardResponse.expMonth)
				&& Objects.equals(this.expYear, stripeCardResponse.expYear)
				&& Objects.equals(this.fingerprint, stripeCardResponse.fingerprint)
				&& Objects.equals(this.funding, stripeCardResponse.funding)
				&& Objects.equals(this.last4, stripeCardResponse.last4)
				&& Objects.equals(this.metadata, stripeCardResponse.metadata)
				&& Objects.equals(this.name, stripeCardResponse.name)
				&& Objects.equals(this.tokenizationMethod, stripeCardResponse.tokenizationMethod);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, object, addressCity, addressCountry, addressLine1, addressLine1Check, addressLine2,
				addressState, addressZip, addressZipCheck, brand, country, cvcCheck, dynamicLast4, expMonth, expYear,
				fingerprint, funding, last4, metadata, name, tokenizationMethod);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class StripeCardResponse {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    object: ").append(toIndentedString(object)).append("\n");
		sb.append("    addressCity: ").append(toIndentedString(addressCity)).append("\n");
		sb.append("    addressCountry: ").append(toIndentedString(addressCountry)).append("\n");
		sb.append("    addressLine1: ").append(toIndentedString(addressLine1)).append("\n");
		sb.append("    addressLine1Check: ").append(toIndentedString(addressLine1Check)).append("\n");
		sb.append("    addressLine2: ").append(toIndentedString(addressLine2)).append("\n");
		sb.append("    addressState: ").append(toIndentedString(addressState)).append("\n");
		sb.append("    addressZip: ").append(toIndentedString(addressZip)).append("\n");
		sb.append("    addressZipCheck: ").append(toIndentedString(addressZipCheck)).append("\n");
		sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    cvcCheck: ").append(toIndentedString(cvcCheck)).append("\n");
		sb.append("    dynamicLast4: ").append(toIndentedString(dynamicLast4)).append("\n");
		sb.append("    expMonth: ").append(toIndentedString(expMonth)).append("\n");
		sb.append("    expYear: ").append(toIndentedString(expYear)).append("\n");
		sb.append("    fingerprint: ").append(toIndentedString(fingerprint)).append("\n");
		sb.append("    funding: ").append(toIndentedString(funding)).append("\n");
		sb.append("    last4: ").append(toIndentedString(last4)).append("\n");
		sb.append("    metadata: ").append(toIndentedString(metadata)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    tokenizationMethod: ").append(toIndentedString(tokenizationMethod)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

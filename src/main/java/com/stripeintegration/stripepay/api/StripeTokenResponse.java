package com.stripeintegration.stripepay.api;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Stripe Token Response object
 */
@ApiModel(description = "Stripe Token Response object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class StripeTokenResponse {
	@JsonProperty("id")
	private String id = null;

	@JsonProperty("object")
	private String object = null;

	@JsonProperty("stripeCardResponse")
	private StripeCardResponse stripeCardResponse = null;

	@JsonProperty("client_ip")
	private Object clientIp = null;

	@JsonProperty("created")
	private BigDecimal created = null;

	@JsonProperty("livemode")
	private Boolean livemode = null;

	@JsonProperty("type")
	private String type = null;

	@JsonProperty("used")
	private Boolean used = null;

	public StripeTokenResponse id(String id) {
		this.id = id;
		return this;
	}

	/**
	 * Stripe token ID
	 * 
	 * @return id
	 **/
	@ApiModelProperty(example = "tok_1FijQuLLyi74k9eCavwYNrpV", value = "Stripe token ID")

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StripeTokenResponse object(String object) {
		this.object = object;
		return this;
	}

	/**
	 * Get object
	 * 
	 * @return object
	 **/
	@ApiModelProperty(value = "")

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public StripeTokenResponse stripeCardResponse(StripeCardResponse stripeCardResponse) {
		this.stripeCardResponse = stripeCardResponse;
		return this;
	}

	/**
	 * Get stripeCardResponse
	 * 
	 * @return stripeCardResponse
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public StripeCardResponse getStripeCardResponse() {
		return stripeCardResponse;
	}

	public void setStripeCardResponse(StripeCardResponse stripeCardResponse) {
		this.stripeCardResponse = stripeCardResponse;
	}

	public StripeTokenResponse clientIp(Object clientIp) {
		this.clientIp = clientIp;
		return this;
	}

	/**
	 * Get clientIp
	 * 
	 * @return clientIp
	 **/
	@ApiModelProperty(value = "")

	public Object getClientIp() {
		return clientIp;
	}

	public void setClientIp(Object clientIp) {
		this.clientIp = clientIp;
	}

	public StripeTokenResponse created(BigDecimal created) {
		this.created = created;
		return this;
	}

	/**
	 * Get created
	 * 
	 * @return created
	 **/
	@ApiModelProperty(value = "")

	@Valid
	public BigDecimal getCreated() {
		return created;
	}

	public void setCreated(BigDecimal created) {
		this.created = created;
	}

	public StripeTokenResponse livemode(Boolean livemode) {
		this.livemode = livemode;
		return this;
	}

	/**
	 * Get livemode
	 * 
	 * @return livemode
	 **/
	@ApiModelProperty(value = "")

	public Boolean isLivemode() {
		return livemode;
	}

	public void setLivemode(Boolean livemode) {
		this.livemode = livemode;
	}

	public StripeTokenResponse type(String type) {
		this.type = type;
		return this;
	}

	/**
	 * Type of the payment
	 * 
	 * @return type
	 **/
	@ApiModelProperty(example = "card", value = "Type of the payment")

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public StripeTokenResponse used(Boolean used) {
		this.used = used;
		return this;
	}

	/**
	 * Get used
	 * 
	 * @return used
	 **/
	@ApiModelProperty(value = "")

	public Boolean isUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		StripeTokenResponse stripeTokenResponse = (StripeTokenResponse) o;
		return Objects.equals(this.id, stripeTokenResponse.id)
				&& Objects.equals(this.object, stripeTokenResponse.object)
				&& Objects.equals(this.stripeCardResponse, stripeTokenResponse.stripeCardResponse)
				&& Objects.equals(this.clientIp, stripeTokenResponse.clientIp)
				&& Objects.equals(this.created, stripeTokenResponse.created)
				&& Objects.equals(this.livemode, stripeTokenResponse.livemode)
				&& Objects.equals(this.type, stripeTokenResponse.type)
				&& Objects.equals(this.used, stripeTokenResponse.used);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, object, stripeCardResponse, clientIp, created, livemode, type, used);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class StripeTokenResponse {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    object: ").append(toIndentedString(object)).append("\n");
		sb.append("    stripeCardResponse: ").append(toIndentedString(stripeCardResponse)).append("\n");
		sb.append("    clientIp: ").append(toIndentedString(clientIp)).append("\n");
		sb.append("    created: ").append(toIndentedString(created)).append("\n");
		sb.append("    livemode: ").append(toIndentedString(livemode)).append("\n");
		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    used: ").append(toIndentedString(used)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

package com.stripeintegration.stripepay.api;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Stripe Payment Public Api
 */
@ApiModel(description = "Stripe Payment Public Api")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-12-04T13:42:16.386Z[GMT]")
public class TokenResponse {
	@JsonProperty("publicAPIkey")
	private String publicAPIkey = null;

	public TokenResponse publicAPIkey(String publicAPIkey) {
		this.publicAPIkey = publicAPIkey;
		return this;
	}

	/**
	 * Stripe public API
	 * 
	 * @return publicAPIkey
	 **/
	@ApiModelProperty(example = "pk_PRUS0R57vq4qK4cPwNlT39Sz005zNQHKxv", value = "Stripe public API")

	public String getPublicAPIkey() {
		return publicAPIkey;
	}

	public void setPublicAPIkey(String publicAPIkey) {
		this.publicAPIkey = publicAPIkey;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TokenResponse tokenResponse = (TokenResponse) o;
		return Objects.equals(this.publicAPIkey, tokenResponse.publicAPIkey);
	}

	@Override
	public int hashCode() {
		return Objects.hash(publicAPIkey);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class TokenResponse {\n");

		sb.append("    publicAPIkey: ").append(toIndentedString(publicAPIkey)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

package com.stripeintegration.stripepay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.model.Customer;
import com.stripeintegration.stripepay.model.DBCustomer;
import com.stripeintegration.stripepay.service.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "payment/customer", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public ResponseEntity<Customer> getStripeCustomerByEmail(@RequestParam(name = "id") int id) {

		Long requestedId = (long) id;
		System.out.println(id);
		DBCustomer dbCustomer = customerService.getCustomerById(requestedId);
		System.out.println("CUSTOMER ID and email:");
		System.out.println(dbCustomer.getCustomerId().toString());
		System.out.println(dbCustomer.getEmail().toString());
		Customer customer = customerService.getCustomerFromStripe(dbCustomer.getCustomerId());

		return ResponseEntity.ok(customer);
	}

}

package com.stripeintegration.stripepay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.model.Card;
import com.stripeintegration.stripepay.api.CardInputBody;
import com.stripeintegration.stripepay.api.CardResponse;
import com.stripeintegration.stripepay.api.DefaultCard;
import com.stripeintegration.stripepay.api.StripeCardApi;
import com.stripeintegration.stripepay.model.DBCustomer;
import com.stripeintegration.stripepay.service.CustomerCardService;
import com.stripeintegration.stripepay.service.CustomerService;
import com.stripeintegration.stripepay.service.PaymentService;
import com.stripeintegration.stripepay.service.StripeService;

@RestController
@CrossOrigin
public class PaymentCardController implements StripeCardApi {

	@Autowired
	StripeService stripeService;

	@Autowired
	CustomerService customerService;

	@Autowired
	PaymentService paymentService;

	@Autowired
	CustomerCardService customerCardService;

	@Override
	@RequestMapping(value = "payment/card/create", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<CardResponse> createNewCardForCustomer(@RequestBody CardInputBody cardInput) {
		CardResponse cardResponse = new CardResponse();

		String token = cardInput.getStripeToken().getId();
		DBCustomer customer = customerService.getCustomer(cardInput.getEmail());

		try {

			Card newCard = stripeService.createCard(token, customer.getCustomerId(),
					cardInput.getStripeToken().getStripeCardResponse());
			customerCardService.createCustomerCard(customer, newCard.getId());
			cardResponse.setMessage("Successfully created card!");
			return new ResponseEntity<CardResponse>(cardResponse, HttpStatus.OK);
		} catch (Exception ex) {
			cardResponse.setMessage(ex.getMessage().toString());
			return new ResponseEntity<CardResponse>(cardResponse, HttpStatus.BAD_REQUEST);
		}

	}

	@Override
	@RequestMapping(value = "payment/card/default", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<CardResponse> setDefaultCreditCardForCustomer(@RequestBody DefaultCard defaultCard) {
		CardResponse cardResponse = new CardResponse();
		DBCustomer customer = customerService.getCustomer(defaultCard.getEmail());
		System.out.println(customer.getEmail());

		try {
			stripeService.setDefaultCreditCard(customer.getCustomerId(), defaultCard.getStripeCardId());

			cardResponse.setMessage("Successfully default card set " + customer.getEmail());
			return new ResponseEntity<CardResponse>(cardResponse, HttpStatus.OK);
		} catch (Exception ex) {
			cardResponse.setMessage(ex.getMessage().toString());
			return new ResponseEntity<CardResponse>(cardResponse, HttpStatus.BAD_REQUEST);
		}

	}
}

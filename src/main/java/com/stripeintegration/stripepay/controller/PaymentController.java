package com.stripeintegration.stripepay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.stripeintegration.stripepay.api.Payment;
import com.stripeintegration.stripepay.api.PaymentResponse;
import com.stripeintegration.stripepay.api.TokenResponse;
import com.stripeintegration.stripepay.service.CustomerService;
import com.stripeintegration.stripepay.service.PaymentService;
import com.stripeintegration.stripepay.service.StripeService;

@RestController
@CrossOrigin
public class PaymentController {

  @Autowired
  StripeService stripeService;

  @Autowired
  CustomerService customerService;

  @Autowired
  PaymentService paymentService;

  // If customer is first time on payment section there are no card details else
  // customer card details will be provided to form as auto input
  @RequestMapping("/payment")
  public ResponseEntity<TokenResponse> payment() {
    TokenResponse token = new TokenResponse();
    token.setPublicAPIkey(StripeService.stripePublicKey);
    return new ResponseEntity<TokenResponse>(token, HttpStatus.OK);
  }

  @RequestMapping(value = "payment/charge", method = RequestMethod.POST,
      consumes = "application/json", produces = "application/json")
  public ResponseEntity<PaymentResponse> chargeCard(@RequestBody Payment paymentDTO) {
    PaymentResponse resp = new PaymentResponse();

    try {
      paymentService.payment(paymentDTO);
      resp.setMessage("Successfully payed!");
      return new ResponseEntity<PaymentResponse>(resp, HttpStatus.OK);
    }

    catch (Exception ex) {
      resp.setMessage(ex.getMessage().toString());
      return new ResponseEntity<PaymentResponse>(resp, HttpStatus.BAD_REQUEST);
    }

  }

}

package com.stripeintegration.stripepay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stripeintegration.stripepay.model.DBCustomer;

@Repository
public interface ICustomer extends JpaRepository<DBCustomer, Long> {

	public DBCustomer findByEmail(String email);

}

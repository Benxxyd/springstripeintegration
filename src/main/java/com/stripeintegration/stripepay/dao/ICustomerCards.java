package com.stripeintegration.stripepay.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stripeintegration.stripepay.model.DBCustomer;
import com.stripeintegration.stripepay.model.DBCustomerCards;

public interface ICustomerCards extends JpaRepository<DBCustomerCards, Long> {

	List<DBCustomerCards> findByCustomer(DBCustomer customer);

}

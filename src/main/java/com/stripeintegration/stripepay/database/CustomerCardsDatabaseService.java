package com.stripeintegration.stripepay.database;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stripeintegration.stripepay.dao.ICustomerCards;
import com.stripeintegration.stripepay.model.DBCustomer;
import com.stripeintegration.stripepay.model.DBCustomerCards;

@Component
public class CustomerCardsDatabaseService {

	@Autowired
	ICustomerCards customerCards;

	public List<DBCustomerCards> getListOfCards(DBCustomer customer) {
		return customerCards.findByCustomer(customer);
	}

	public DBCustomerCards saveCustomerCard(DBCustomer customer, String cardId) {
		DBCustomerCards cards = new DBCustomerCards();
		cards.setCustomer(customer);
		cards.setCard(cardId);
		return customerCards.save(cards);
	}

}

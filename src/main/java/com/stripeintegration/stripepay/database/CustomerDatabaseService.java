package com.stripeintegration.stripepay.database;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stripeintegration.stripepay.dao.ICustomer;
import com.stripeintegration.stripepay.model.DBCustomer;

@Component
public class CustomerDatabaseService {

	@Autowired
	private ICustomer customerDAO;

	public DBCustomer saveCustomer(String email, String stripeCustomerId) {
		DBCustomer customer = new DBCustomer();

		customer.setEmail(email);
		customer.setCustomerId(stripeCustomerId);
		return customerDAO.save(customer);
	}

	public DBCustomer getStripeCustomerId(String email) {
		DBCustomer customer = customerDAO.findByEmail(email);
		return customer;

	}

	public DBCustomer getCustomerById(Long id) {
		Optional<DBCustomer> customer = customerDAO.findById(id);
		DBCustomer newCustomer = customer.get();
		return newCustomer;
	}

}

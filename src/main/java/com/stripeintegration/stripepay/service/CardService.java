package com.stripeintegration.stripepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripe.model.Card;
import com.stripeintegration.stripepay.api.CardInputBody;
import com.stripeintegration.stripepay.model.DBCustomer;

@Service
public class CardService {

	@Autowired
	StripeService stripeService;

	@Autowired
	CustomerService customerService;

	@Autowired
	PaymentService paymentService;

	public Card createCard(String token, DBCustomer customer, CardInputBody cardDTO) throws Exception {

		stripeService.getStripeCustomer(customer.getCustomerId());
		Card card = stripeService.createCard(token, customer.getCustomerId(),
				cardDTO.getStripeToken().getStripeCardResponse());
		return card;

	}
}

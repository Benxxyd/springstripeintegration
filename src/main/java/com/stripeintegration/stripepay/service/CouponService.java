package com.stripeintegration.stripepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripe.model.Coupon;

@Service
public class CouponService {

	@Autowired
	StripeService stripeService;

	public Coupon createCoupon(String percent_off, String duration, int duration_in_months) {
		return stripeService.createCoupon(percent_off, duration, duration_in_months);

	}

}

package com.stripeintegration.stripepay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripeintegration.stripepay.database.CustomerCardsDatabaseService;
import com.stripeintegration.stripepay.model.DBCustomer;
import com.stripeintegration.stripepay.model.DBCustomerCards;

@Service
public class CustomerCardService {

	@Autowired
	private CustomerCardsDatabaseService customerCardsDBservice;

	public Object createCustomerCard(DBCustomer customer, String cardId) {
		return customerCardsDBservice.saveCustomerCard(customer, cardId);
	}

	public List<DBCustomerCards> getCustomerCards(DBCustomer customer) {
		return customerCardsDBservice.getListOfCards(customer);
	}

}

package com.stripeintegration.stripepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripeintegration.stripepay.database.CustomerDatabaseService;
import com.stripeintegration.stripepay.model.DBCustomer;

@Service
public class CustomerService {

	@Autowired
	private CustomerDatabaseService customerDatabaseService;

	public DBCustomer createCustomer(String email, String stripeCustomerId) {
		return customerDatabaseService.saveCustomer(email, stripeCustomerId);
	}

	public DBCustomer getCustomer(String email) {
		return customerDatabaseService.getStripeCustomerId(email);
	}

	public DBCustomer getCustomerById(Long id) {
		return customerDatabaseService.getCustomerById(id);
	}

	public Customer getCustomerFromStripe(String customerId) {
		System.out.println("USO U CUSTOMER");
		System.out.println(customerId);
		Customer customer = null;

		try {
			customer = Customer.retrieve(customerId);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return customer;
	}

}

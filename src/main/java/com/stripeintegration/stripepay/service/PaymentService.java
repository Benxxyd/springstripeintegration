package com.stripeintegration.stripepay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stripe.model.Card;
import com.stripe.model.Customer;
import com.stripe.model.Invoice;
import com.stripeintegration.stripepay.api.Payment;
import com.stripeintegration.stripepay.model.DBCustomer;

@Service
public class PaymentService {

  @Autowired
  StripeService stripeService;

  @Autowired
  private CustomerService customerService;

  @Autowired
  private CustomerCardService customerCardService;

  public Invoice payment(Payment paymentDTO) throws Exception {


    String token = paymentDTO.getStripeToken().getId();
    Double amount = paymentDTO.getPrice();
    DBCustomer existingCustomer = customerService.getCustomer(paymentDTO.getEmail());

    Invoice invoice = null;

    if (existingCustomer != null) {

      // charge customer
      stripeService.chargeNewCard(existingCustomer.getCustomerId(), amount);
      invoice = stripeService.createInvoice(existingCustomer.getCustomerId(), amount.intValue());

    } else {

      // creates and retrieves customer from stripe
      Customer customer = stripeService.createCustomer(paymentDTO.getEmail());

      // creates customer in DB (userid, stripeCustomerId)
      DBCustomer dbCustomer =
          customerService.createCustomer(paymentDTO.getEmail(), customer.getId());

      Card card = stripeService.createCard(token, customer.getId(),
          paymentDTO.getStripeToken().getStripeCardResponse());

      customerCardService.createCustomerCard(dbCustomer, card.getId());

      // charge customer
      stripeService.chargeNewCard(customer.getId(), amount);

      invoice = stripeService.createInvoice(dbCustomer.getCustomerId(), amount.intValue());
    }

    stripeService.sendInvoiceByEmail(invoice);
    return invoice;
  }

}

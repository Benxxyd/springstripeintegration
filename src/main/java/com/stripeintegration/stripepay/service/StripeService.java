package com.stripeintegration.stripepay.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.exception.CardException;
import com.stripe.exception.StripeException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Coupon;
import com.stripe.model.Customer;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceItem;
import com.stripeintegration.stripepay.api.StripeCardResponse;

@Service
public class StripeService {

	public static String stripePublicKey;
	private static String stripeSecretKey;

	@Autowired
	public StripeService(@Value("${stripePublicApiKey}") String stripePublicApiKey,
			@Value("${stripeSecretApiKey}") String stripeSecretApiKey) {
		StripeService.stripeSecretKey = stripeSecretApiKey;
		StripeService.stripePublicKey = stripePublicApiKey;
		Stripe.apiKey = stripeSecretKey;
	}

	public Charge chargeNewCard(String customerId, double amount) throws Exception {
		Map<String, Object> chargeParams = new HashMap<String, Object>();

		chargeParams.put("amount", (int) (amount * 100));
		chargeParams.put("currency", "USD");
		chargeParams.put("customer", customerId);

		Charge charge = null;

		try {
			charge = Charge.create(chargeParams);

		} catch (CardException e) {
			// Since it's a decline, CardException will be caught
			System.out.println("Status is: " + e.getCode());
			System.out.println("Message is: " + e.getMessage());

		}

		return charge;
	}

	public Customer createCustomer(String email) throws Exception {
		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("description", "Customer for " + email);
		customerParams.put("email", email);

		Customer customer = null;
		try {

			// create a new customer
			customer = Customer.create(customerParams);

		} catch (Exception ex) {
			// Since it's a decline, CardException will be caught
			System.out.println("Status is: " + ((StripeException) ex).getCode());
			System.out.println("Message is: " + ex.getMessage());
		}
		return customer;
	}

	public Invoice createInvoice(String customerId, Integer price) {

		Map<String, Object> invoiceItemParams = new HashMap<>();
		invoiceItemParams.put("amount", price);
		invoiceItemParams.put("currency", "USD");
		invoiceItemParams.put("customer", customerId);
		invoiceItemParams.put("description", "One-time setup fee");

		Map<String, Object> invoiceParams = new HashMap<>();
		invoiceParams.put("customer", customerId);
		invoiceParams.put("collection_method", "send_invoice");
		invoiceParams.put("days_until_due", 30);

		Invoice invoice = null;
		try {
			InvoiceItem.create(invoiceItemParams);
			invoice = Invoice.create(invoiceParams);
			invoice.finalizeInvoice();

		} catch (StripeException e) {
			e.printStackTrace();
		}

		return invoice;
	}

	public void sendInvoiceByEmail(Invoice invoice) {
		try {
			invoice.sendInvoice();
		} catch (StripeException ex) {
			// Since it's a decline, CardException will be caught
			System.out.println("Status is: " + ex.getCode());
			System.out.println("Message is: " + ex.getMessage());
		}
	}

	public Card createCard(String token, String customerId, StripeCardResponse paymentDTOTokenCard) {

		Customer customer = null;
		Card newCard = null;
		try {
			customer = Customer.retrieve(customerId);
		} catch (StripeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Map<String, Object> cardParams = new HashMap<>();
		cardParams.put("source", token);
		try {
			newCard = (Card) customer.getSources().create(cardParams);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newCard;
	}

	public Customer setDefaultCreditCard(String customerId, String stripeCardId) {

		Customer customer = null;

		try {
			customer = Customer.retrieve(customerId);
		} catch (StripeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Map<String, Object> customerParams = new HashMap<>();
		customerParams.put("default_source", stripeCardId);

		try {
			customer = customer.update(customerParams);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return customer;
	}

	public Customer getStripeCustomer(String customerId) {
		Customer customer = null;
		try {
			customer = Customer.retrieve(customerId);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return customer;
	}

	public Coupon createCoupon(String percent_off, String duration, int duration_in_months) {

		Map<String, Object> couponParams = new HashMap<>();
		couponParams.put("percent_off", percent_off);
		couponParams.put("duration", duration);
		couponParams.put("duration_in_months", duration_in_months);
		Coupon coupon = null;
		try {
			coupon = Coupon.create(couponParams);
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return coupon;

	}

}

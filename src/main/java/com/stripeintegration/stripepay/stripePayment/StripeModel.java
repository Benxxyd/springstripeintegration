package com.stripeintegration.stripepay.stripePayment;

public class StripeModel {

	private StripeTokenModel stripeTokenModel;
	private int amount;

	public StripeTokenModel getStripeTokenModel() {
		return stripeTokenModel;
	}

	public void setStripeTokenModel(StripeTokenModel stripeTokenModel) {
		this.stripeTokenModel = stripeTokenModel;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "StripeModel [stripeTokenModel=" + stripeTokenModel + ", amount=" + amount + "]";
	}

}

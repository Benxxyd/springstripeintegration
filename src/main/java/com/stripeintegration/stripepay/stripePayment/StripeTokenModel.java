package com.stripeintegration.stripepay.stripePayment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StripeTokenModel {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("object")
	@Expose
	private String object;
	@SerializedName("card")
	@Expose
	private StripeCardModel stripeCardModel;
	@SerializedName("client_ip")
	@Expose
	private Object clientIp;
	@SerializedName("created")
	@Expose
	private Integer created;
	@SerializedName("livemode")
	@Expose
	private Boolean livemode;
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("used")
	@Expose
	private Boolean used;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public StripeCardModel getCard() {
		return stripeCardModel;
	}

	public void setCard(StripeCardModel stripeCardModel) {
		this.stripeCardModel = stripeCardModel;
	}

	public Object getClientIp() {
		return clientIp;
	}

	public void setClientIp(Object clientIp) {
		this.clientIp = clientIp;
	}

	public Integer getCreated() {
		return created;
	}

	public void setCreated(Integer created) {
		this.created = created;
	}

	public Boolean getLivemode() {
		return livemode;
	}

	public void setLivemode(Boolean livemode) {
		this.livemode = livemode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}

}

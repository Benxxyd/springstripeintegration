package com.stripeintegration.stripepay;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestInterfaceDemo implements TestLifecycleLogger {
  @Test
  void isEqualValue() {
    assertEquals(1, "a".length(), "is always equal");
  }

}
